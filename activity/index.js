let number = 100;
console.log("The number you provided is " + number);

for (let n = number; n >= 0; n--){
	
	if (n % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue
	}

	if(n % 5 === 0){
		console.log(n);
		continue;
	}

	if(n <= 50){
		break;
	}
}

let string = "supercalifragilisticexpialidocious";
let cons = "";

for(i = 0; i < string.length; i++){
	if(
		string[i] === "a" ||
		string[i] === "e" ||
		string[i] === "i" ||
		string[i] === "o" ||
		string[i] === "u" ){
		continue
	}
	else {
		cons += string[i]
	}
}
console.log(cons)


