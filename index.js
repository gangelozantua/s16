/*function displayMsgToSelf(){	
	for (let i = 0; i < 10; i ++) {
		console.log("Hi " + (i + 1));
	}
}

displayMsgToSelf();
*/


//Try-Catch-Finally Statement

/*
	- "try catch" statements are commonly used for error handling
	
	- there are instances when the application returns an error/warning that is not necessarily an error in the context of our code
	
	- these errors are a result of an attempt of the programming language to help developer in creating efficient code
	
	- they are used to specify a response whenever an exception/error is received
	
	- it is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statements
	
	- in programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
	
	- the "finally" block is used to specify a response/action that is used to handle/resolve errors.
}
*/

/*function showIntensityAlert(windSpeed){
	try {
		//attempt to execute a code
		alert(determineTyphoonInentsity(windSpeed));

	//error are commonly used variable names used by developers for storing errors
	} catch(error) {
		// "typeof" operator is used to check the data type of a value/expression and returns a string value of what the type is
		console.log(typeof error);
		// catch errors within "try" statement
		//in this case the error is an unknown function "alert" which does not exist in JS
		// the "alert" function is used similarily to a prompt to alert the user
		// "error.message" is used to access the information relating to an error object
		console.log(error.message);

	} finally {
		// continue execution of code regardless of success and failure or code execution in the "try" block to handle/resolve error
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);*/

/*const num = 100, x = "a";
console.log(num);

try {
	console.log(num / x);
	console.log(a);
} catch (error) {
	console.log("An error occured.");
	console.log("Error Message: " + error);
} finally {
	alert("Finally will execute.");
}*/


/*function gradeEvaluator(grade){
	try{
		if(grade <= 70){
			console.log("F");
		} else if(grade >= 71 && grade <= 79){
			console.log("C");
		} else if(grade >= 80 && grade <= 89){
			console.log("B");
		} else if(grade >= 90){
			console.log("A");
		} else if(typeof grade !== "number"){
			throw Error("Not a number");
		}
	}catch(error){
		console.log(error);
	}finally {
		alert("Execute");
	}
}
*/



/*prof solution ???????

function gradeEvaluator(grade){
	try{
		if(grade >=90) {
			return "A";
		} else if(grade >= 80) {
			return "B";
		} else if(grade >=71) {
			return "C";
		} else if(grade >=70) {
			return "F";
		} else if(typeof grade !== "number") {
			throw Error("Invalid Number");
		}
	} catch(error) {
		console.log(error);
	} finally {
		alert("Let's continue!");
	}
}

gradeEvaluator(90);
*/

function displayMsgtoSelf(){
	console.log("Hi");
}

displayMsgtoSelf();

let count = 10;

while(count !== 0){
	displayMsgtoSelf();
	count--;
}

/*let isGender = false;

while()
while(isGender === true){
	console.log("while code block")
}// will not rune

*/



//Do-While Loop
	//Do While loop is similar to While loop. However Do While loop will run the code block at least once before it checks the condition.

	//with While loop, we first check the condition and then run the code block/statements

let doWhileCounter = 20;

do {
	console.log(doWhileCounter)
	--doWhileCounter
} while (doWhileCounter < 0);


//For loop
	//A For loop is more flexible than While and Do-While loops.
	//it consists of 3 parts:
		// 1. The "initialization" value that will track the progression of the loop
		// 2. the "expression/condition" that will be evaluated which will determine whether the loop will run one more time or not.
		// 3. the " finalExpression" indicated how to advance the loop

	/* Syntax
		
		for(initialization,condition,finalExpression){
			//codeblock / statement
		}
	*/

	//create a loop that will start from 1 and end at 20

	for(let x = 1; x <= 20; x++){
		console.log(x);
	}

// Q: Can we use For loop in a string? -yes

let name = "Nikko Handsome";

//Accessing elements of a string
	// using index
	// index starts at 0

console.log(name[0]);//N
console.log(name[1]);//i
console.log(name[2]);//k

//Count of the string charactiers
	//using .length

console.log(name.length);//5

//Use For loop to display each character of the string

for(let index = 0; index < name.length; ++index){
	console.log(name[index]);// N I 2K O "" H A N D S O M E
}

//Using For loop in an array

let fruits = ["mango", "apple", "orange", "banana", "strawberries", "kiwi"];
	//elements - each value inside the array	

console.log(fruits);

//total count of elements in an array (.length)
console.log(fruits.length);//6

//access each element in an array
console.log(fruits[0]);//mango
console.log(fruits[4]);//strawberry
console.log(fruits[5]);//kiwi

//how do we determine the last element in an array if we don't know the total number of the elements.

//console.log(fruits.length);//6
console.log(fruits[fruits.length - 1]);

//if we want to assign new value to an element
	//assignment operator
fruits[6] = "grapes";
console.log(fruits);//7 fruits (updated)

//Using For loop in an array
/*for(let i = 0; i < fruits.length; i++){
	console.log(fruits[i]);
}// 7 fruits
*/


//example of array of objects
let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "Luxury Sedan"
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}
];

console.log(cars.length);//3
console.log(cars[1]);//rolls royce/luxury sedan

for(let c = 0; c < cars.length; c++){
	console.log(cars[c]);
}


/*let myName = "ANGELO ZANTUA";

for(let n = 0; n < myName.length; n++){
	if(
		myName[n].toLowerCase() === "a" ||
		myName[n].toLowerCase() === "e" ||
		myName[n].toLowerCase() === "i" ||
		myName[n].toLowerCase() === "o" ||
		myName[n].toLowerCase() === "u"){
		console.log(3);
	}else {
		console.log(myName[n]);
	}
}
*/
// Continue and Break
	// The "continue" statement allows the code to go to the next iteration of the loop "without finishing the execution of the following statements in a code block"
		// - skips the current loop ans proceed to the next loop

for(let a = 20; a > 0; a--){
	if(a % 2 === 0){
		continue
	}

	if(a < 10){
		break
	}
	console.log(a);
}



let string = "ALEXANDRO";

for(let i = 0; i < string.length; i++){
	if (string[i].toLowerCase() === "a"){
		console.log("Continue");
		continue;
	}
	if (string[i].toLowerCase() === "d"){
		break;
	}
}

// Q: Is it possible to have another loop inside a loop? - yes

//Nested Loops

	for(let x = 0; x <= 10; x++){
		console.log(x);

		for(let y = 0; y <= 10; y++){
		console.log(`${x} + ${y} = ${x + y}` );
		}
	}














